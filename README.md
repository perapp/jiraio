# jiraio

Python tool to import an Excel backlog into JIRA.

## Usage
To run, install Python 3.6 or higher and run:

```
pip install -r src/python/requirements.txt
python src/python/jiraio/jiraio.py --url https://jira.myhost.com -u MyUsername -p SuperSecretPass import-file PathToMyExcelFile
```

## Excel format

The Excel document should either:
  * Contain a sheet called Backlog or PBL
  * or have the active sheet contain the backlog

The first row of the Excel sheet should contain column header and atleast have the columns:
  * JIRA Ref - If this is blank, a new story will be created and the cell updated with the new JIRA key.
  * Project - Project to create issue in
  * Issue Type - e.g. Story
  * Summary
  * Epic Link - Description of Epic. If epic doesn't exist it will be created with issue type 'Epic'.

The remaining rows will be imported into JIRA.
