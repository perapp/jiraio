APP_NAME=jiraio

build:
	docker build -t $(APP_NAME) -f src/docker/app.docker .

run:
	docker run --rm -ti $(APP_NAME)
