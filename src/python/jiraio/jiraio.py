"""
Python script to import Jira stories from Excel
The Excel sheet should either:
  * Contain a sheet called Backlog or PBL
  * or have the active sheet contain the backlog

The first row of the Excel sheet should contain column header and atleast have the columns:
  * JIRA Ref - If this is blank, a new story will be created and the cell updated with the new JIRA key.
  * Project - Project to create issue in
  * Issue Type - e.g. Story
  * Summary
  * Epic Link - Description of Epic. If epic doesn't exist it will be created with issue type 'Epic'.

"""

import click
from jira.client import GreenHopper
from jira.client import JIRA
from atlassian import Jira
import openpyxl
import functools

class JiraIO(object):
    def __init__(self, url, username, password):
        self.jira = Jira(url, username, password)
        self.fields = {x['name']: x['id']
                       for x in self.jira.get_all_custom_fields()}
        
    def import_file(self, path):
        wb = openpyxl.load_workbook(path)
        ws = wb.active
        for x in wb:
            if x.title.lower() in {'backlog', 'pbl'}:
                ws = x
        
        ws.save_workbook = functools.partial(wb.save, path)
        ws.save_workbook() # Make sure we can save file before we start processing
        jira_ref_col, mapping = self.process_header(ws)
        self.process_rows(ws, jira_ref_col, mapping)

    def process_header(self, ws):
        # figure out which columns to synch with Jira by looking at first row
        mapping = []
        lower_fields = {x.lower(): x
                        for x in self.fields.keys()}
        header_row = first(ws.iter_rows(max_row=1, values_only=True))
        jira_ref_col = None
        for i, header in enumerate(header_row):
            if header.lower() in lower_fields:
                mapping.append((lower_fields[header.lower()], i))
            elif header.lower() == 'jira ref':
                jira_ref_col = i
        if jira_ref_col is None:
            raise Exception('No "JIRA Ref" column in Excel sheet. Please add and try again')
        
        return jira_ref_col, mapping

    def process_rows(self, ws, jira_ref_col, mapping):
        # process rrows in Excel
        for row in ws.iter_rows(min_row=2):
            jira_ref = row[jira_ref_col].value

            values = {}
            for (field_name, col) in mapping:
                value = row[col].value
                if value is None:
                    continue
                if field_name == "Project":
                    value = {"key": value}
                elif field_name == "Issue Type":
                    value = {"name": value}
                values[field_name] = value
            if not values['Summary']:
                continue

            values["Epic Link"] = self.lookup_or_create_epic(values["Project"]["key"], values["Epic Link"])

            jira_values = {self.fields[x]:y
                           for x, y in values.items()}
            if not jira_ref:
                jira_ref = self.issue_create(jira_values)
                print(f"Created story {jira_ref}")
                row[jira_ref_col].value = jira_ref
                ws.save_workbook()
            
    def find_by_summary(self, project_key, summary):
        for item in self.jira.jql(f'project="{project_key}" and summary~"{summary}"')['issues']:
            if item['fields'][self.fields['Summary']] == summary:
                yield item
    
    def lookup_or_create_epic(self, project_key, name):
        """
        Lookup or create epic. Return epic key.
        """
        try:
            epic = first([x for x in self.find_by_summary(project_key, name)
                          if x["fields"][self.fields["Issue Type"]]["name"] == "Epic"])
            return epic["key"]
        except ValueError:
            return self.issue_create({self.fields['Project']: {"key": project_key},
                                      self.fields['Issue Type']: {'name': 'Epic'},
                                      self.fields['Epic Name']: name,
                                      self.fields['Summary']: name})

    def issue_create(self, fields):
        """
        Create new issue and return key. Raise Exception on error.
        """
        x = self.jira.issue_create(fields)
        if x.get("errors") or x.get("errorMessages"):
            print(x["errorMessages"])
            print(x["errors"])
            raise Exception(f"Error creating JIRA issue: {x['errors']}")
        return x["key"]

    def export(self):
        pass

def first(xs):
    for x in xs:
        return x
    raise ValueError('No first element')

@click.group()
@click.option('-l', '--url')
@click.option('-u', '--user', envvar='USER')
@click.option('-p', '--password')
@click.pass_context
def cli(ctx, url, user, password):
    ctx.obj = JiraIO(url, user, password)

@cli.command()
@click.argument('fpath', type=click.Path(exists=True))
@click.pass_obj
def import_file(jio, fpath):
    jio.import_file(fpath)
    
if __name__ == '__main__':
    cli()
